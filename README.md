# E-Journal Config File (EZProxy) Generator

### Background / User Story / Use Case
Each morning, DUL Technical Services needs to have daily generated file generated that lists URL resources  
from SerialsSolultions, in response to "360-link" changes.  
  
A resulting file, `e-journals.cfg` needs to be created/generated and copied to a location recognizable by DUL's EZProxy 
application.  
  
`e-journals.cfg` is the file DUL uses to expose electronic (library) resources to its patrons, students and staff.

## Project Scope
* This application generates the `e-journals.cfg` file, and copies it to an acceptable location for EZProxy to read.
* This application does not restart EZProxy when it completes its operation

## Sustainability (Technical, etc)
Coming soon...

## FTP Transfer No Longer Support
The code that facilitated FTP transfers from **ftp.serialssolutions.com** has been removed.

## New CLI Flags
`-sfile` is the lone file to process, or  
`-sdir` is the directory that contains one or more files that could be parsed for SerialsSolutions data.  
  
`-noftp` has been deprecated  
  
## Legacy
This project supercedes and replaces a longstanding PERL script originally created by Ken Mitchell (formerly of ITS).  In Core Services' 
pursuit of exploring technology trends, we elected to use `go` to rewrite the original script.  
  
This approach allows Core Service to deploy a standalone executable (binary) file that is virtually free of 
external dependencies.  
  
## Installing on Linux
### Redhat/CentOS

First, install Go:  
`sudo yum install golang`  

Next, clone this project (set up SSH/GPG keys as needed):  
`git clone git@gitlab.oit.duke.edu:dul-its/ezp360confgen.git`  
  
Before you build, you'll need to do a few "go get" housekeeping things:  
```sh
go get -u golang.org/x/text/encoding/unicode
go get -u gopkg.in/ini.v1
go get -u github.com/secsy/goftp
go get -u github.com/gocarina/gocsv
```
  
Finally...  
`cd /path/to/cloned/ezp360confgen ; go build -i -o ezp360confgen .`
  
Before you run it, however, create a config file named `my.ini` containing:  
```txt
debug = false
prohibited_hosts = library.duke.edu
prohibited_domains = duke.edu
restricted_hosts = heinonline.org,global.factiva.com,factiva.com,uptodate.com
proxyprefix_re = "https?://login.proxy.lib.duke.edu/login\\?url=(.+)$"

# files to download, separated by commas
ftp_paths = daily_report.zip

[ftp]
host = ftp.serialssolutions.com

# contact Core Services to get these credentials
user = [redacted]
pass = [redacted]
```
  
### Building/Running the Application  
**Build**  
`go build -i -o ezp360confgen`  
- without the "-o" file, the executable would be called "main", I believe.  
  
**Run**  
  
With no options  
`./ezp360confgen`  
  
With your own rolled up INI file...    
`./ezp360confgen -ini /path/to/your/ini`  
  
`./ezp360confgen -h`, produces...  
```txt
Usage of ./ezp360confgen:
  -configonly
        display config settings and exit
  -debug
        display debug verbosity
  -ini string
        Location of INI (Config) file... (default "my.ini")
  -out string
        filename to send output to (instead of STDOUT) (default "e-journals.cfg")
```