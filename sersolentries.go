package main

import (
	"regexp"
	"strings"
	"fmt"
	_ "errors"
	"net/url"
	_ "os"
)

type SerSolEntries struct {
	PrefixFilterRE *regexp.Regexp
	
	PrefixTargetField string
	
	ProhibitedHosts []string
	
	ProhibitedDomains []string
	
	Rows []map[string]string
	
	ResourceEntries map[string][]SerSolEntry
}

func (s *SerSolEntries) String() (string) {
	var sb strings.Builder
	for k, v := range s.ResourceEntries {
		sb.WriteString(fmt.Sprintf("Key: %s\n", k))
		for _, vv := range v {
			sb.WriteString(fmt.Sprintf("%s\n", vv))
		}
		sb.WriteString(fmt.Sprintln())
		sb.WriteString(fmt.Sprintln())
	}
	return sb.String()
	
}

type SerSolEntry struct {
	ResourceName string
	ResourceTarget string
	ResourceIdentifier string
	ResourceType string
	ResourceDomain string
	ResourceScheme string
	ResourceHost string
	ResourcePort string
	UniquePath string
}

func NewSerSolEntries(prefixRE *regexp.Regexp, target string, prohibitedHosts []string, prohibitedDomains []string) (*SerSolEntries) {
	d := &SerSolEntries{PrefixTargetField: "URL"}
	if len(target) > 0 {
		d.PrefixTargetField = target
	}
	if prefixRE != nil {
		d.PrefixFilterRE = prefixRE
	}
	d.ProhibitedDomains = prohibitedDomains
	d.ProhibitedHosts = prohibitedHosts
	d.ResourceEntries = make(map[string][]SerSolEntry)
	return d
}

func (sd *SerSolEntries) TryAddingMapEntry(m map[string]string) (error) {
	
	if sd.PrefixFilterRE == nil {
		sd.Rows = append(sd.Rows, m)
		return nil
	}
	
	// Get the submatches based on the designated prefix
	submatches := sd.PrefixFilterRE.FindStringSubmatch(m[sd.PrefixTargetField])
	if (len(submatches) == 0) {
		return fmt.Errorf("Discovered an invalid entry for [%s]", m[sd.PrefixTargetField])
	}
	proxiedUrl := submatches[len(submatches)-1]

	// This is the first place where we 
	// check if this entry is from a valid host/domain
	u, err := url.Parse(proxiedUrl)
	if err != nil {
		fmt.Errorf("%s\n", err)
	}
	if u == nil  {
		return fmt.Errorf("invalid host string detected [%s]", proxiedUrl)
	}
	parts := strings.Split(u.Hostname(), ".")
	if len(parts) < 2 {
		return nil
	}
	
	var domain string
	if len(parts) > 3 {
		// Crude attempt to detect international domains
		domain = strings.Join(parts[len(parts)-3:], ".")
	} else {
		domain = strings.Join(parts[len(parts)-2:], ".")
	}
	
	serSolEntry := SerSolEntry{}
	if m["Type"] == "Book" {
		// Book
		if val, ok := m["ISBN10"]; ok {
			serSolEntry.ResourceIdentifier = val
		} else {
			serSolEntry.ResourceIdentifier = m["ISBN13"]
		}
	} else {
		// Journal
		if val, ok := m["eISSN"]; ok {
			serSolEntry.ResourceIdentifier = val
		} else {
			serSolEntry.ResourceIdentifier = m["ISSN"]
		}
	}
	serSolEntry.ResourceTarget = m["Resource"]
	serSolEntry.ResourceName = m["Title"]
	serSolEntry.ResourceType = m["Type"]
	serSolEntry.ResourceDomain = domain
	serSolEntry.ResourceHost = u.Hostname()
	serSolEntry.ResourcePort = u.Port()
	serSolEntry.ResourceScheme = u.Scheme
	serSolEntry.UniquePath = fmt.Sprintf("%s:%s:%s", u.Hostname(), u.Port(), u.Scheme)

	if _, ok := sd.ResourceEntries[proxiedUrl]; !ok {
		sd.ResourceEntries[proxiedUrl] = make([]SerSolEntry, 0)
	}
	sd.ResourceEntries[proxiedUrl] = append(sd.ResourceEntries[proxiedUrl], serSolEntry)
	
	return nil
}

