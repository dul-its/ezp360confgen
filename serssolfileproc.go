package main

import (
	"os"
	"io"
	"encoding/csv"
	"fmt"
	"regexp"
	"log"
)

type SersSolFileProcessor struct {
	files []string
	
	Config SerSolConfig
	
	// Map of processed "Unique Hosts" where 
	// each one is marked as a boolean true
	ResourceHostPathEntries map[string]bool
	
	baseDir	string
}

func (ss *SersSolFileProcessor) AddFilesFromDirFileInfo(base string, d []os.FileInfo) (error) {
	ss.baseDir = base
	for _, fi := range d {
		if (!fi.IsDir()) {
			ss.files = append(ss.files, fmt.Sprintf("%s/%s", base, fi.Name()))
		}
	}
	return nil
}

func (ss *SersSolFileProcessor) AddFiles(f []string) {
	ss.files = append(ss.files, f...)
}

func (ss *SersSolFileProcessor) ProcessForStanzas(out *os.File) (error) {

	// load files from archive if needed
	
	for _, filename := range ss.files {
		reader, err := os.Open(filename)
		defer reader.Close()
		if err != nil {
			return err
		}
		fmt.Printf("processing contents of %s...\n", filename)	
		serSolEntries := NewSerSolEntries(ss.Config.PrefixPattern, "URL", ss.Config.ProhibitedDomains, ss.Config.ProhibitedHosts)
		
		err = ss.FindEntriesFromFile(reader, serSolEntries)
		if err != nil {
			return err
		}
		err = ss.WriteStanzas(serSolEntries, out)
		if err != nil {
			return err
		}
	}
	return nil
}

func (ss *SersSolFileProcessor) FindEntriesFromFile(reader io.Reader, sse *SerSolEntries) (error) {
	headerRE := regexp.MustCompile("[A-Za-z0-9]+")
	
	r := csv.NewReader(reader)	
	
	var header []string
	
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		if header == nil {
			//record[0] = strings.Replace(record[0], "ï»¿", "", 1)			
			// remove any unicode jibber-jabber from any of the header cells
			record[0] = headerRE.FindString(record[0])
			header = record

		} else {
			dict := map[string]string{}
			for i := range header {
				dict[header[i]] = record[i]
			}
			
			err := sse.TryAddingMapEntry(dict)
			if (err != nil && gDebug) {
				if gDebug {
					log.Println(err)
				}
				return err
			}
		}
	}
	return nil
}


func (p *SersSolFileProcessor) WriteStanzas(sse *SerSolEntries, out *os.File) (error) {

	for resUrl, resName := range sse.ResourceEntries {		
		if len(resName) == 0 && gDebug {
			log.Printf("No resource entries for [%s]\n", resUrl)
			continue
		}
		
		canProceed := true
		for _, host := range sse.ProhibitedHosts {
			matchedHost, err := regexp.Match(host, []byte(resName[0].ResourceHost))
			if (err != nil) {
				panic(err)
				return err
			}
			if matchedHost {
				if (gDebug) {
					log.Printf("found prohibited host [%s]\n", resName[0].ResourceHost)
				}
				canProceed = false
			}
		}
		
		for _, domain := range sse.ProhibitedDomains {
			matchedDomain, err := regexp.Match(domain, []byte(resName[0].ResourceHost))
			if (err != nil) {
				panic(err)
				return err
			}
			if matchedDomain {
				if (gDebug) {
					log.Printf("found prohibited host [%s]\n", resName[0].ResourceHost)
				}
				canProceed = canProceed && false
			}
		}
		
		if canProceed {
			firstResource := resName[0]
			if _, ok := p.ResourceHostPathEntries[firstResource.UniquePath]; !ok {
				
				stanz := StanzaEntry{ResourceUrl: resUrl, ConfigEntry: "", Restricted: false}
				stanz.ParseSerSolEntry(firstResource)
				p.ResourceHostPathEntries[firstResource.UniquePath] = true
				fmt.Fprintf(out, "%s\n", stanz)
				log.Printf("stanza entry written for: %s\n", firstResource.UniquePath)
			}
		}
	}	
	log.Println("#### end of ResourceEntries processing...")
	return nil
}