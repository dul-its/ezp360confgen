package main

import (
	_"fmt"
	"io"
	"encoding/csv"
	"log"
	_ "reflect"
	"github.com/gocarina/gocsv"
	_ "golang.org/x/text/encoding/unicode"
	_ "bufio"
	"regexp"
	
)

type SerSolReportFile struct {
	Data []byte
	Header []string
	Entries *SerSolEntries
}

func (s *SerSolReportFile) Write(buf []byte) (int, error) {
	s.Data = append(s.Data, buf...)
	return len(buf), nil	
}

func (s *SerSolReportFile) ProcessFileWithConfig(reader io.Reader, cfg SerSolConfig) {
	
	resources := []*SerSolResource{}
	
	if err := gocsv.Unmarshal(reader, &resources); err != nil { // Load clients from file
		panic(err)
	}
	for _, resource := range resources {
		log.Printf("%s | %s | %s\n", resource.ResType, resource.Title, resource.Resource)
	}
}

// based from
// https://gist.github.com/drernie/5684f9def5bee832ebc50cabb46c377a
func (s *SerSolReportFile) ReadFileAndProcessWithConfig(reader io.Reader, cfg SerSolConfig) {

	r := csv.NewReader(reader)
	
	entries := NewSerSolEntries(cfg.PrefixPattern, "URL", cfg.ProhibitedDomains, cfg.ProhibitedHosts)
	
	re := regexp.MustCompile("[A-Za-z0-9]+")
	
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if s.Header == nil {
			//record[0] = strings.Replace(record[0], "ï»¿", "", 1)			
			// remove any unicode jibber-jabber from any of the header cells
			record[0] = re.FindString(record[0])
			s.Header = record

		} else {
			dict := map[string]string{}
			for i := range s.Header {
				dict[s.Header[i]] = record[i]
			}

			// Don't append it to "rows", but rather
			// determine if it meets the criteria for safekeeping
			// sersolResource := NewSerSolResourceFromDict(dict)
			
			//rows = append(rows, dict)
			err := entries.TryAddingMapEntry(dict)
			if (err != nil) {
				log.Println(err)
			}
		}
	}
	s.Entries = entries
}
