package main

import (
	"archive/zip"
	"fmt"
	"io"
	"log"
	"regexp"
	"os"
	"encoding/csv"
	_ "strings"
)

type SerSolConfig struct {
	// default file extension for files inside the ZIP
	FileExtension string
	
	// For columns representing a URL, this regexp is 
	// the prefix that matches a valid entry
	UrlPrefixRE regexp.Regexp
	
	// Prohibited hosts
	ProhibitedHosts []string
	
	// Prohibited domain
	ProhibitedDomains []string
	
	// Extract files from the ZIPs into this directory
	TmpPath string
	
	// PrefixPattern
	PrefixPattern *regexp.Regexp
}

type SerSolZipFileProcessor struct {
	files []string
	
	Config SerSolConfig
	
	// Map of processed "Unique Hosts" where 
	// each one is marked as a boolean true
	ResourceHostPathEntries map[string]bool
	
	ValidEntries []map[string]string
}

func (p *SerSolZipFileProcessor) AddFiles(f []string) {
	p.files = append(p.files, f...)
}

func (p *SerSolZipFileProcessor) WriteStanzasFromZipContents(out *os.File) (error) {
	// Start a ZipReader and read CSV files
	//_ = os.Mkdir(p.Config.TmpPath, os.ModeDir)
	
	//if err := os.Chdir(c.TmpPath); err != nil {
	//	panic(err)
	//}

	cfg := p.Config
	//p.SerSolEntries = NewSerSolEntries(cfg.PrefixPattern, "URL", cfg.ProhibitedDomains, cfg.ProhibitedHosts)
	
	for i := 0; i < len(p.files); i++ {
		
		serSolEntries := NewSerSolEntries(cfg.PrefixPattern, "URL", cfg.ProhibitedDomains, cfg.ProhibitedHosts)
		// Open ZIP file for processing
		r, err := zip.OpenReader(p.files[i])
		defer r.Close()
		if err != nil {
			log.Fatal(err)
		}	
		
		// For each file in the ZIP file....
		for _, f := range r.File {			
			validFileExt, err := regexp.Match(p.Config.FileExtension, []byte(f.Name))
			if err != nil {
				log.Fatal(err)
			}
			if !validFileExt {
				log.Printf("This file (%s) does not appear to be a valid entry\n", f.Name)
				continue
			}
			
			rc, err := f.Open()
			defer rc.Close()
			if err != nil {
				return err
			}
			fmt.Printf("processing contents of %s:\n", f.Name)	
			p.FindEntriesFromFile(rc, serSolEntries)	
			p.WriteStanzas(serSolEntries, out)	
			//p.ReadFileAndProcess(rc)
		}
	}
	
	return nil
}

func (p *SerSolZipFileProcessor) FindEntriesFromFile(reader io.Reader, sse *SerSolEntries) {
	re := regexp.MustCompile("[A-Za-z0-9]+")
	
	r := csv.NewReader(reader)	
	
	var header []string
	
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if header == nil {
			//record[0] = strings.Replace(record[0], "ï»¿", "", 1)			
			// remove any unicode jibber-jabber from any of the header cells
			record[0] = re.FindString(record[0])
			header = record

		} else {
			dict := map[string]string{}
			for i := range header {
				dict[header[i]] = record[i]
			}

			err := sse.TryAddingMapEntry(dict)
			if (err != nil && gDebug) {
				log.Println(err)
			}
		}
	}
}

/*
func (p *SerSolZipFileProcessor) ReadFileAndProcess(reader io.Reader) {
	re := regexp.MustCompile("[A-Za-z0-9]+")
	
	r := csv.NewReader(reader)	
	
	var header []string
	
	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}
		if header == nil {
			//record[0] = strings.Replace(record[0], "ï»¿", "", 1)			
			// remove any unicode jibber-jabber from any of the header cells
			record[0] = re.FindString(record[0])
			header = record

		} else {
			dict := map[string]string{}
			for i := range header {
				dict[header[i]] = record[i]
			}

			err := p.SerSolEntries.TryAddingMapEntry(dict)
			if (err != nil && gDebug) {
				log.Println(err)
			}
		}
	}
}
*/

func (p *SerSolZipFileProcessor) WriteStanzas(sse *SerSolEntries, out *os.File) (error) {

	for resUrl, resName := range sse.ResourceEntries {		
		if len(resName) == 0 {
			log.Printf("No resource entries for [%s]\n", resUrl)
			continue
		}
		
		canProceed := true
		for _, host := range sse.ProhibitedHosts {
			matchedHost, err := regexp.Match(host, []byte(resName[0].ResourceHost))
			if (err != nil) {
				panic(err)
			}
			if matchedHost {
				//log.Printf("found prohibited host [%s]\n", resName[0].ResourceHost)
				canProceed = false
			}
		}
		
		for _, domain := range sse.ProhibitedDomains {
			matchedDomain, err := regexp.Match(domain, []byte(resName[0].ResourceHost))
			if (err != nil) {
				panic(err)
			}
			if matchedDomain {
				//log.Printf("found prohibited host [%s]\n", resName[0].ResourceHost)
				canProceed = canProceed && false
			}
		}
		
		if canProceed {
			firstResource := resName[0]
			if _, ok := p.ResourceHostPathEntries[firstResource.UniquePath]; !ok {
				
				stanz := StanzaEntry{ResourceUrl: resUrl, ConfigEntry: "", Restricted: false}
				stanz.ParseSerSolEntry(firstResource)
				p.ResourceHostPathEntries[firstResource.UniquePath] = true
				fmt.Fprintf(out, "%s\n", stanz)
			}
		}
	}	
	log.Println("#### end of ResourceEntries processing...")
	return nil
}