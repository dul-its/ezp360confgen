package main

import (
	"fmt"
	"os"	
	"io/ioutil"
	_"time"
	"log"
	"flag"
	"regexp"
	"gopkg.in/ini.v1"
	_"strings"
)

// Need a regexp expression for http(s)?://login.proxy.lib.duke.edu
var proxyPrefixRE = regexpExt{regexp.MustCompile("https?:login.proxy.lib.duke.edu/login\\?url=")}
var gDebug bool
var gOsStanzaFile *os.File

func main() {
	
	// parse CLI flags
	var (
		configOnly 		bool
		outFilename		string
		iniFilename 	string
		noFTP			bool
		localFilename	string
		sourceDir		string
		sourceFile		string
	)
	
	flag.BoolVar(&gDebug, "debug", false, "display debug verbosity")
	flag.BoolVar(&configOnly, "configonly", false, "display config settings and exit")
	flag.StringVar(&iniFilename, "ini", "my.ini", "Location of INI (Config) file...")
	flag.StringVar(&outFilename, "out", "e-journals.cfg", "filename to send output to (instead of STDOUT).")
	flag.StringVar(&sourceFile, "sfile", "", "single file to process. Overrides -sdir option.")
	flag.StringVar(&sourceDir, "sdir", "", "directory containing files to process." )
	flag.BoolVar(&noFTP, "noftp", false, "don't connect to FTP site")
	flag.StringVar(&localFilename, "localfile", "daily_report.zip", "name of local file when not using FTP.")
	flag.Parse()
	
	if len(sourceFile) == 0 && len(sourceDir) == 0 {
		flag.Usage()
		os.Exit(1)
	}
	
	// read config and merge
	cfg, err := ini.Load(iniFilename)
	if err != nil {
		fmt.Printf("ezp360confgen: failed to read: %v", err)
		os.Exit(1)
	}
	
	// Print the config settings to STDOUT and exit 
	// when --configonly is present
	if configOnly {
		fmt.Printf("%+v", cfg)
		os.Exit(0)
	}
	
	prohibitedHosts := cfg.Section("").Key("prohibited_hosts").Strings(",")
	prohibitedDomains := cfg.Section("").Key("prohibited_domains").Strings(",")
	prefixPattern := regexp.MustCompile("https?://login.proxy.lib.duke.edu/login\\?url=(.+)$")
		
	sconfig := SerSolConfig{
		FileExtension: ".csv",
		UrlPrefixRE: *proxyPrefixRE.Regexp, 
		ProhibitedHosts: prohibitedHosts,
		ProhibitedDomains: prohibitedDomains,
		TmpPath: "tmp",
		PrefixPattern: prefixPattern,
	}
		
	processor := &SersSolFileProcessor{Config: sconfig}
	processor.ResourceHostPathEntries = make(map[string]bool)
	
	if len(sourceFile) > 0 {
		processor.AddFiles([]string{sourceFile})
	} else if len(sourceDir) > 0 {
		
		// use ioutil to read the 'sourceDir' and add all files to the processor
		files, err := ioutil.ReadDir(sourceDir)
		if err != nil {
			log.Fatal(err)
		}
		
		processor.AddFilesFromDirFileInfo(sourceDir, files)
	}
	
	
	// Open the specified output file that will eventually have 
	// "stanza" entries written to it
	gOsStanzaFile, err := os.OpenFile(outFilename, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("writing to output file [%s]\n", gOsStanzaFile.Name())
	
	// Pass the *os.File to the processing function, 
	// sit back and enjoy the show.
	err = processor.ProcessForStanzas(gOsStanzaFile)
	if err != nil {
		log.Fatal(err)
	}
	
	log.Println("done.")
}
