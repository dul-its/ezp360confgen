package main

import (
)

type EJournalStanzasMap struct {
	
}

type SerSolResource struct {
	ResType string `csv:"Type"`
	Title string `csv:"Title"`
	FullURL string `csv:"URL"`
	Resource string `csv:"Resource"`
	ISBN10 string `csv:"ISBN10"`
	ISBN13 string `csv:"ISBN13"`
	Author string `csv:"Author"`
	Publication string `csv:"Publication"`
	ISSN string `csv:"ISSN"`
	EISSN string `csv:"eISSN"`
}

type IsSerSolResource interface {
	
}

func (e *EJournalStanzasMap) ProcessLines(s *SerSolReportFile) {
	
}