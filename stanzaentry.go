package main

import (
	"fmt"
	"strings"
)

type StanzaEntry struct {
	ResourceUrl string
	Title string
	Domain string
	Hostname string
	Protocol string
	ConfigEntry string
	AdditionalTitles []string
	Restricted bool
}

// Set a stanza's title, clipping it to 70 characters and 
// appending an elipsis when necessary...
func (se *StanzaEntry) SetTitle(t string) {
	if len(t) > 70 {
		se.Title = fmt.Sprintf("%s...", t[0:70])
	} else {
		se.Title = t
	}
}

// Produces string output that is suitable 
// for writing in an EZProxy-aware stanzas file
func (se StanzaEntry) String() (string) {
	var sb strings.Builder
	sb.WriteString(fmt.Sprintf("Title\t%s\n", se.Title))
	sb.WriteString(fmt.Sprintf("URL\t%s\n", se.ResourceUrl))
	
	// Don't include the protocol in HJ
	sb.WriteString(fmt.Sprintf("HJ\t%s\n", strings.Replace(se.Hostname, "https://", "", 1)))
	sb.WriteString(fmt.Sprintf("DJ\t%s\n", se.Domain))
	//if se.Protocol == "https" {
	//	sb.WriteString(fmt.Sprintf("HJ\t%s://%s\n", se.Protocol, se.Hostname))
	//} else {
	//	sb.WriteString(fmt.Sprintf("HJ\t%s\n", se.Hostname))
	//}
	return sb.String()
}

// Parse (gather, rather) the data from a SerSolEntry record
// and copy the necessary parts to this StanzaEntry
func (se *StanzaEntry) ParseSerSolEntry(e SerSolEntry) {
	se.SetTitle(e.ResourceName)
	se.Domain = e.ResourceDomain
	se.Hostname = e.ResourceHost
	se.Protocol = e.ResourceScheme
}
