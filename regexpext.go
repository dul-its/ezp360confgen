package main

import (
	"regexp"
)

// It's a long name for an Interface
type RegexpWithNamedCaptures interface {
	FindStringSubmatchMap(s string) map[string]string
}

// http://blog.kamilkisiel.net/blog/2012/07/05/using-the-go-regexp-package/
type regexpExt struct {
	*regexp.Regexp
}

func (r *regexpExt) FindStringSubmatchMap(s string) map[string]string {
  captures := make(map[string]string)

  match := r.FindStringSubmatch(s)
  if match == nil {
      return captures
  }

  for i, name := range r.SubexpNames() {
      // Ignore the whole regexp match and unnamed groups
      if i == 0 || name == "" {
          continue
      }
      
      captures[name] = match[i]

  }
  return captures
}